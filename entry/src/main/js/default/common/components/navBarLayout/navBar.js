export default {
    props :{
        backgroundColor:{
            default :"#fff"
        },
        height:{
            default : "50px"
        },
        position: {
            default : "fixed"
        }
    },
    data: {

    }
}
