
export default {
    props:{
      title : {
          default:"标题标题标题标题标题标题标题标题标题"
      },
      content1 : {
          default:"内容这是content1内容这是ent1内容这内容这是content1内容这是是content1内容这是content1内容这是content1内容"
      },
      content1Color:{
          default: "#333"
      },
      content2 : {
          default:""
      },
      titleFontSize : {
            default:"20px"
      },
      content1FontSize : {
          default : "18px"
      },
      content1Color : {
          default : "#999"
      },
        imgUrl:{
            default : "common/images/bg-tv.jpg"
      },
      titleFontWeight:{
          default:700
      }
    },
    data(){
        return{
            content1Text : this.content1,
            isShow : false
        }
    },
    onLayoutReady(){
        console.log("cardListItem")
        const w =  this.$refs["cont2"].getBoundingClientRect().width;
        const h = this.$refs["cont2"].getBoundingClientRect().height;
        let len = regString(/[px|rem|fp|\%|em|vm]+/g,(this.content1FontSize)
                 .toString())
                 .toString().length;
        const fontSize = +((this.content1FontSize).toString()   //字体大小
                  .slice(0,(this.content1FontSize).length - len));
        let content = this.content1.toString();
        const content1Len =  content.length;
        let skipIndex = 0
        let strNum = Math.floor((w / fontSize))  * (Math.floor((h / fontSize)) - 2);
        const tempNum = strNum;
        for(let i = 0; i < tempNum; i++){ //处理英文数字字符
            console.log(`"i="${i} temp = ${tempNum} `);
            const s = regString(/[A-z|0-9]+/g,content);
            if(!s) break;
            skipIndex = content.indexOf(s) + s.toString().length;
            console.log(`skpi ${skipIndex}`);
            content = content.slice(skipIndex);
            i += skipIndex;
            strNum += s.toString().length;
        }
        if(strNum < content1Len){
            this.content1Text = (this.content1Text).toString()
                    .slice(0,strNum);
            this.isShow = true;
            console.log(this.content1Text)
        }
   },

}

function regString(rege,str){
    const reg = new RegExp(rege);

    return   reg.exec(str);




}