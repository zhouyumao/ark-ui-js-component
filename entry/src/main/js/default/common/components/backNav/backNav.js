/*
    组件BackNav
    props ：backgroundColor 组件背景颜色 default：#fff 赋值方式 #xxxxxx|english|rgb|rgba
            backName  组件返回按钮名字 default ：’‘|string  绑定点击事件 childBackClick 触发抛给父组件
            title 组件标题 default : "标题" | string
            height 组件高度 default ：“50px” 赋值方式 同css
            weigh  标题字体加粗 default ：700
            left 、 center 、right  default ：true  可根据需求更改Boolean值 true 显示 false 隐藏
            childMoreClick  更多按钮点击事件 抛给父组件
 */



export default {
    props:{
        backgroundColor:{
            default:"#fff"
        },
        backName : {
            default : ' '
        },
        title:{
            default:"标题"
        },
        height:{
            default: "50px"
        },
        weigh:{
            default:700
        },
        left:{
            default: true
        },
        center:{
            default: true
        },
        right:{
            default: true
        },
        position:{
            default : "fixed"
        }
    },
    data: {
    },
    childBackClick(){
        this.$emit("childBackClick",'back触发');
    },
    childMoreClick(){
        this.$emit("childMoreClick","more触发");
    }
}
