import prompt from '@system.prompt';

export default {
    data: {
        title:"",
        menu:[
            {
                icon :  "shouye.png",
                active :"active_shouye.png",
                title: "标签1"
            },
            {
                icon: "gouwuche.png",
                active: "active_gouwuche.png",
                title: "标签2"
            },
            {
                icon :  "xiangmu.png",
                active :"active_xiangmu.png",
                title: "标签3"
            },
            {
                icon :  "wo.png",
                active :"active_wo.png",
                title: "标签4"
            }
        ]
    },
    backDump(e){
        prompt.showToast({
            message: e._detail
        })
    },
    pullMore(e){
        prompt.showToast({
            message: e._detail
        })
    },
    setTitle(e){
        this.title = e._detail;

    },
    onInit(){
        this.title = this.menu[0].title;
    }


}
